package data.access.repositories;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.w3c.dom.Document;

import data.access.models.City;
import data.access.models.Flight;
import data.access.models.User;

public class CityDAO {
	
	private SessionFactory factory;
	
	public CityDAO(SessionFactory factory) {
		this.factory = factory;
	}
	
	@SuppressWarnings("unchecked")
	public City addCity(City city) {
		int cityId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			int responseCode = 0;
			try {
				String api = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + URLEncoder.encode(city.getName(), "UTF-8") + "&sensor=true";
			    URL url = new URL(api);
			    HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
			    httpConnection.connect();
			    responseCode = httpConnection.getResponseCode();
			    if(responseCode == 200)
			    {
			      DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();;
			      Document document = builder.parse(httpConnection.getInputStream());
			      XPathFactory xPathfactory = XPathFactory.newInstance();
			      XPath xpath = xPathfactory.newXPath();
			      XPathExpression expr = xpath.compile("/GeocodeResponse/status");
			      String status = (String)expr.evaluate(document, XPathConstants.STRING);
			      if(status.equals("OK"))
			      {
			         expr = xpath.compile("//geometry/location/lat");
			         String latitude = (String)expr.evaluate(document, XPathConstants.STRING);
			         expr = xpath.compile("//geometry/location/lng");
			         String longitude = (String)expr.evaluate(document, XPathConstants.STRING);
			         city.setLatitude(Double.parseDouble(latitude));
			         city.setLongitude(Double.parseDouble(longitude));
			        // return new String[] {latitude, longitude};
			      }
			      else
			      {
			         throw new Exception("Error from the API - response status: "+status);
			      }
			    } 
			} catch(Exception e) {
		    	e.printStackTrace();
		    }
		    
			cityId = (Integer) session.save(city);
			city.setId(cityId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return city;
	}
	
	@SuppressWarnings("unchecked")
	public City findCity(String cityName) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> cities = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE name = :cityName");
			query.setParameter("cityName", cityName );
			cities = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return cities != null && !cities.isEmpty() ? cities.get(0) : null;
	}
}
