package data.access.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.*;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import data.access.repositories.CityDAO;
import data.access.repositories.FlightDAO;
import data.access.repositories.UserDAO;

@Entity
@Table(name = "User")
public class User {
	
	@Id @GeneratedValue
	@Column(name = "id")
	private int id;
	
	@Column(name = "userName")
	private String userName;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "lastName")
	private String lastName;
	
	@Column(name = "userType")
	private boolean userType;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public boolean isUserType() {
		return userType;
	}
	public void setUserType(boolean userType) {
		this.userType = userType;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	 public static void main(String[] args){
		 
		 SessionFactory factory = new Configuration().configure().buildSessionFactory();
		 
		 
		 Session session = factory.openSession();
			Transaction tx = null;
			
			User u = new User();
			u.setFirstName("Dumitru");
			u.setLastName("Casap");
			u.setPassword("1234");
			u.setUserName("casap");
			u.setUserType(false);
			
			User u1 = new User();
			u1.setFirstName("Leo");
			u1.setLastName("Messi");
			u1.setPassword("123456");
			u1.setUserName("leomessi");
			u1.setUserType(true);
			
			City c1 = new City();
			c1.setName("Berlin");
			c1.setLatitude(30.5);
			c1.setLongitude(74.5);
			
			City c2 = new City();
			c2.setName("Bucuresti");
			c2.setLatitude(33.5);
			c2.setLongitude(50.3321);
			
			City c3 = new City();
			c2.setName("Cluj");
			c2.setLatitude(332.5);
			c2.setLongitude(530.3321);
			
			Flight f1 = new Flight();
			f1.setAirplaneType("Good Type");
			f1.setFlightNumber("BQ325152");
			Date date1 = new Date();
			f1.setDepartureDate(date1);
			f1.setArrivalDate(date1);
			f1.setArrivalCity(c1);
			f1.setDepartureCity(c2);
			try {
				tx = session.beginTransaction();
				//FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
				//Flight fff = flightDAO.findFlight(7);
				//List<Flight> fff = flightDAO.findFlights();
				//UserDAO userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
				//userDAO.login("Dumitru", "Casap");
				//Flight f111 = flightDAO.findFlight(7);
				//Flight f2 = flightDAO.addFlight(f1);
				/*City c1 = new City();
				c1.setId(4);
				c1.setName("Cluj-Napoca");
				c1.setLatitude(40.5);
				c1.setLongitude(80.31);
				session.delete(c1);*/

				/*session.save(c1);
				session.save(c2);
				session.save(c3);
				session.save(u1);
				session.save(u);
				session.save(f1);*/
				CityDAO cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
				cityDAO.addCity(c1);
				
				tx.commit();
			} catch (Exception e) {
				if (tx != null) {
					tx.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
			
	 }
	 
	}
