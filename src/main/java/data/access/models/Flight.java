package data.access.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Flight")
public class Flight {
	
	@Id @GeneratedValue
	@Column(name = "id")
	private int id;
	
	@Column(name = "flightNumber")
	private String flightNumber;
	
	@Column(name = "airplaneType")
	private String airplaneType;
	
	@OneToOne(fetch = FetchType.EAGER)
	//@JoinColumn(name="id")
	private City departureCity;
	
	@Column(name = "departureDate")
	private Date departureDate;
	
	@OneToOne(fetch = FetchType.EAGER)
	//@JoinColumn(name="id")
	private City arrivalCity;
	
	@Column(name = "arrivalDate")
	private Date arrivalDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getAirplaneType() {
		return airplaneType;
	}
	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}
	public City getDepartureCity() {
		return departureCity;
	}
	public void setDepartureCity(City departureCity) {
		this.departureCity = departureCity;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public City getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
}
