package bussines.services;

import org.hibernate.cfg.Configuration;

import data.access.models.User;
import data.access.repositories.UserDAO;

public class UserLogin {
	public User Login(String username, String password) {
		
		User user = null;
		UserDAO userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());	
		user = userDAO.login(username, password);
		return user;
	}
}
