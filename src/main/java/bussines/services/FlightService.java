package bussines.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.hibernate.cfg.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import data.access.models.Flight;
import data.access.repositories.FlightDAO;
import data.access.repositories.UserDAO;

public class FlightService {
	
	public List<Flight> getAllFlights() {
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		
		List<Flight> flights = flightDAO.findFlights();
		return flights;
	}
	
	public Flight addFlight(Flight flight) {
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		
		Flight f = flightDAO.addFlight(flight);
		return f;
	}
	
	public Flight updateFlight(Flight flight) {
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		
		Flight f = flightDAO.updateFlight(flight);
		return f;
	}
	
	public Flight findFlight(int id) {
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		
		Flight f = flightDAO.findFlight(id);
		return f;
	}
	
	public Flight findFlightByNumber(String number) {
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		
		Flight f = flightDAO.findFlightByNumber(number);
		return f;
	}
	
	public Flight deleteFlight(int id) {
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		
		Flight f = flightDAO.deleteFlight(id);
		return f;
	}
	
	public String getOffset(String latitude, String longitude) throws IOException, ParserConfigurationException, SAXException {
        String urlStr = "http://new.earthtools.org/timezone/" + latitude + "/" + longitude;

        URL url = new URL(urlStr);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/xml");

        InputStream xml = connection.getInputStream();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(xml);

        NodeList localTime = document.getElementsByTagName("offset");

        return localTime.item(0).getTextContent();
    }
}
