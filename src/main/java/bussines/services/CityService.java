package bussines.services;

import org.hibernate.cfg.Configuration;

import data.access.models.City;
import data.access.repositories.CityDAO;


public class CityService {
	
	public City findCityByName(String cityName) {
		City city= null;
		CityDAO cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());	
		city = cityDAO.findCity(cityName);
		return city;
	}
	
}
