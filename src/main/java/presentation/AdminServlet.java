package presentation;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import com.mysql.cj.core.util.StringUtils;

import bussines.services.CityService;
import bussines.services.FlightService;
import data.access.models.City;
import data.access.models.Flight;

import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = { "/admin" })
public class AdminServlet extends HttpServlet {

	
   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
      
	   String action = request.getParameter("action");
	   
	   if(action!=null) {
		   if(action.equalsIgnoreCase("deleteFlight")) {
			   System.out.println("haaaaa");
			   int id = Integer.parseInt(request.getParameter("id"));
			   FlightService flightService = new FlightService();
			   Flight flight = flightService.deleteFlight(id);
		   }
	   }
	   
	   HttpSession session = request.getSession(false);
	   FlightService flightService = new FlightService();
	   List<Flight> flights = flightService.getAllFlights();
	   request.setAttribute("flights", flights);
	   request.getRequestDispatcher("admin.jsp").include(request, response);

   }
   
   public void doPost(HttpServletRequest request, HttpServletResponse response)
		      throws ServletException, IOException {
	   	String flightNumber = request.getParameter("flightNumber");
		String airplaneType = request.getParameter("airplaneType");
		String departureCity = request.getParameter("departureCity");
		String departureDate = request.getParameter("departureDate");
		String arrivalCity = request.getParameter("arrivalCity");
		String arrivalDate = request.getParameter("arrivalDate");
		
		CityService cityService = new CityService();
		
		
		if(request.getParameter("add")!=null) {
			City c1 = cityService.findCityByName(departureCity);
			City c2 = cityService.findCityByName(arrivalCity); 
			Flight flight = new Flight();
			flight.setAirplaneType(airplaneType);
			flight.setFlightNumber(flightNumber); 
		    Date d1 = new Date();
		    Date d2 = new Date();
		    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		    try {
				d1 = df.parse(arrivalDate);
				d2 = df.parse(departureDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
			flight.setArrivalDate(d1);
			flight.setDepartureDate(d2);
			
			if(c1!=null){
				flight.setDepartureCity(c1);
			} else {
				City c = new City();
				c.setName(departureCity);
				flight.setDepartureCity(c);
			}
			
			if(c2!=null){
				flight.setArrivalCity(c2);
			} else {
				City c = new City();
				c.setName(arrivalCity);
				flight.setArrivalCity(c);
			}
			
			FlightService flightService = new FlightService();
			flightService.addFlight(flight);
				
		}
		
		if(request.getParameter("update")!=null) {
			FlightService flightService = new FlightService();
			Flight flight = flightService.findFlightByNumber(flightNumber);
			
			if(flight!=null){
				City c1 = cityService.findCityByName(departureCity);
				City c2 = cityService.findCityByName(arrivalCity); 

				flight.setAirplaneType(airplaneType);
				flight.setFlightNumber(flightNumber); 
			    Date d1 = new Date();
			    Date d2 = new Date();
			    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
			    try {
					d1 = df.parse(arrivalDate);
					d2 = df.parse(departureDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    
				flight.setArrivalDate(d1);
				flight.setDepartureDate(d2);
				
				if(c1!=null){
					flight.setDepartureCity(c1);
				} else {
					City c = new City();
					c.setName(departureCity);
					flight.setDepartureCity(c);
				}
				
				if(c2!=null){
					flight.setArrivalCity(c2);
				} else {
					City c = new City();
					c.setName(arrivalCity);
					flight.setArrivalCity(c);
				}
				
				flightService.updateFlight(flight);
			}
			
		}
		
		
		response.sendRedirect(request.getContextPath()+"/admin");
		
   }
   
}
