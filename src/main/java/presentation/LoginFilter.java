package presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import data.access.models.User;


@WebFilter("/*")
public class LoginFilter implements Filter {

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {    
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String url = ((HttpServletRequest)request).getRequestURL().toString();

        List<String> items = Arrays.asList(url.split("/"));
        chain.doFilter(request, response);
        
        if(items.get(items.size()-1).equals("admin") || items.get(items.size()-1).equals("client")) {
        	User user = (User) session.getAttribute("user");
            if(user!=null) {
            	if(user.isUserType() && items.get(items.size()-1).equals("admin")) {
            		
                } else if(user.isUserType() == false && items.get(items.size()-1).equals("client")) {

                }
                else {
                    response.sendRedirect(request.getContextPath());
                }
            } else {
            	response.sendRedirect(request.getContextPath());
            }
        }
      
    }

	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}


	public void destroy() {
		// TODO Auto-generated method stub
		
	}


}
