package presentation;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.mysql.cj.core.util.StringUtils;

import bussines.services.CityService;
import bussines.services.FlightService;
import data.access.models.City;
import data.access.models.Flight;

import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = { "/client" })
public class ClientServlet extends HttpServlet {

	
   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
	   
	   String action = request.getParameter("action");
	   
	   if(action!=null) {
		   if(action.equalsIgnoreCase("queryLocalTime")) {
			   int id = Integer.parseInt(request.getParameter("id"));
			   FlightService flightService = new FlightService();
			   Flight flight = flightService.findFlight(id);
			   
			try {
				String s1 = flightService.getOffset(String.valueOf(flight.getDepartureCity().getLatitude()), String.valueOf(flight.getDepartureCity().getLongitude()));
				String s2 = flightService.getOffset(String.valueOf(flight.getArrivalCity().getLatitude()), String.valueOf(flight.getArrivalCity().getLongitude()));
				int i1 = Integer.parseInt(s1);
				int i2 = Integer.parseInt(s2);
				Date d1 = new Date(flight.getDepartureDate().getTime() + 3600 * (i2-i1)*1000);
				Date d2 = new Date(flight.getArrivalDate().getTime() + 3600 * (i1-i2)*1000);
				request.setAttribute("departureLocalTime", d1);
				request.setAttribute("arrivalLocalTime", d2);
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   }
	   }
	   HttpSession session = request.getSession(false);
	   FlightService flightService = new FlightService();
	   List<Flight> flights = flightService.getAllFlights();
	   request.setAttribute("flights", flights);
	   request.getRequestDispatcher("client.jsp").include(request, response);
       
   }
   
   public void doPost(HttpServletRequest request, HttpServletResponse response)
		      throws ServletException, IOException {
		      	
   }
}