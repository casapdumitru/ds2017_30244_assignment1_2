package presentation;

import java.io.*;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import bussines.services.FlightService;
import bussines.services.UserLogin;
import data.access.models.Flight;
import data.access.models.User;

import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
 
 
   public void deGet(HttpServletRequest request, HttpServletResponse response)
		      throws ServletException, IOException {
	   request.setAttribute("error", "");
	}
   public void doPost(HttpServletRequest request, HttpServletResponse response)
		      throws ServletException, IOException {
		      
		      String username = request.getParameter("username");
		      String password = request.getParameter("password");
		      
		      UserLogin userLogin = new UserLogin();      
		      User user = userLogin.Login(username, password);
		      
		      if(user != null) {
		    	  request.getSession().setAttribute("error", "");
		    	  request.getSession().setAttribute("user", user);
		    	  if(user.isUserType() == true) {
		              response.sendRedirect(request.getContextPath() + "/admin");
		              return;
		    	  }
		    	  else
		              response.sendRedirect(request.getContextPath() + "/client");
		              return;
		      } else {
		    	  request.getSession().setAttribute("error", "Error to login");
		    	  response.sendRedirect(request.getContextPath());
		      }
   }
}
