<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Client</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
  <div style="margin-bottom: 40px" class="col-sm-12">
  	<div class="col-sm-6">
  		<h2>CLIENT PAGE!</h2>
  	</div>
  	<div class="col-sm-6">
  		<h2> ${user.firstName} ${user.lastName} welcome to this application! </h2>
  	</div>
  </div>

  <h3 style="text-align: center">Flights list</h3>
  <table border=1 class=table>
  	<thead>
  		<tr>
			<th> Flight Number </th>
			<th> Airplane Type </th>
			<th> Departure City </th>
			<th> Departure Date </th>
			<th> Arrival City </th>
			<th> Arrival Date </th>
			<th> Query Local Times </th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${flights}" var="flight">
		    <tr>
		      <td><c:out value="${flight.flightNumber}" /></td>
		      <td><c:out value="${flight.airplaneType}" /></td>
		      <td><c:out value="${flight.departureCity.name}" /></td>
		      <td><c:out value="${flight.departureDate}" /></td>
		      <td><c:out value="${flight.arrivalCity.name}" /></td>
		      <td><c:out value="${flight.arrivalDate}" /></td>
		      <td style="text-align: center; cursor: pointer"><a href="client?action=queryLocalTime&id=<c:out value="${flight.id}"/>">Query</a></td>
		    </tr>
	  	</c:forEach>
	</tbody>
	</table>
	<br/>
	<br/>
	 <p> ${departureLocalTime}</p>		
	 <p> ${arrivalLocalTime}</p>	
	 
  </div>	
</body>
</html>